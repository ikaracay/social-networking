package com.hsbc.challenge.sn.user.controller;

import com.hsbc.challenge.sn.exception.UnprocessableEntityException;
import com.hsbc.challenge.sn.exception.UserIdentifierNotFoundException;
import com.hsbc.challenge.sn.message.model.Message;
import com.hsbc.challenge.sn.message.model.MessageDTO;
import com.hsbc.challenge.sn.user.helper.RequestBodyBuilder;
import com.hsbc.challenge.sn.user.helper.RequestHeaderBuilder;
import com.hsbc.challenge.sn.user.helper.RequestUrlBuilder;
import com.hsbc.challenge.sn.user.service.UserService;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by 44082315 on 28/01/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerMockTest {

    private static final Logger logger = LoggerFactory.getLogger(UserControllerMockTest.class);
    private static final ResultHandler logResultHandler = result -> logger.info(result.getResponse().getContentAsString());

    private static final String USER_IDENTIFIER = "Loremipsu";
    private static final String FOLLOW_USER_IDENTIFIER = "loremip";
    private static final String POST_MESSAGE_TEXT = "Lorem ipsum dolor sit amet, cons";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Test
    public void shouldPostMessageSuccessfully() throws Exception {

        willDoNothing().given(userService).postMessage(anyString(), anyString());

        mockMvc.perform(post(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(POST_MESSAGE_TEXT))
        ).andDo(logResultHandler)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.result", Is.is(not(nullValue()))))
                .andExpect(jsonPath("$.result", is("ok")));

        then(userService).should().postMessage(eq(USER_IDENTIFIER), eq(POST_MESSAGE_TEXT));
    }

    @Test
    public void shouldPostMessageThrowExceptionWhenUserServiceThrowsException() throws Exception {

        willThrow(RuntimeException.class).given(userService).postMessage(anyString(), anyString());

        mockMvc.perform(post(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(POST_MESSAGE_TEXT))
        ).andDo(logResultHandler)
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        then(userService).should().postMessage(eq(USER_IDENTIFIER), eq(POST_MESSAGE_TEXT));
    }

    @Test
    public void shouldGetMyMessagesReturnAllMessagesOfUserIdentifierSuccessfully() throws Exception {

        final List<MessageDTO> messages = new ArrayList<>();
        messages.add(new MessageDTO(new Message("userId", "userIdentifier", "textMessage")));
        given(userService.findMyMessages(anyString())).willReturn(messages);

        mockMvc.perform(get(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
        ).andDo(logResultHandler)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.messages.*", hasSize(1)))
                .andExpect(jsonPath("$.messages[0].text", is("textMessage")))
                .andExpect(jsonPath("$.messages[0].userIdentifier", is("userIdentifier")));

        then(userService).should().findMyMessages(eq(USER_IDENTIFIER));
    }

    @Test
    public void shouldGetMyMessagesThrowExceptionWhenUserServiceThrowsUserIdentifierNotFoundException() throws Exception {

        willThrow(UserIdentifierNotFoundException.class).given(userService).findMyMessages(anyString());

        mockMvc.perform(get(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(POST_MESSAGE_TEXT))
        ).andDo(logResultHandler)
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        then(userService).should().findMyMessages(eq(USER_IDENTIFIER));
    }

    @Test
    public void shouldFollowUserSuccessfully() throws Exception {

        willDoNothing().given(userService).followUser(anyString(), anyString());

        mockMvc.perform(post(RequestUrlBuilder.buildFollowUserUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildFollowUserRequestBody(FOLLOW_USER_IDENTIFIER))
        ).andDo(logResultHandler)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.result", Is.is(not(nullValue()))))
                .andExpect(jsonPath("$.result", is("ok")));

        then(userService).should().followUser(eq(USER_IDENTIFIER), eq(FOLLOW_USER_IDENTIFIER));
    }

    @Test
    public void shouldFollowUserThrowsIdentifierNotFoundExceptionWhenUserServiceThrowsUserIdentifierNotFound() throws Exception {

        willThrow(UserIdentifierNotFoundException.class).given(userService).followUser(anyString(), anyString());

        mockMvc.perform(post(RequestUrlBuilder.buildFollowUserUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildFollowUserRequestBody(FOLLOW_USER_IDENTIFIER))
        ).andDo(logResultHandler)
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        then(userService).should().followUser(eq(USER_IDENTIFIER), eq(FOLLOW_USER_IDENTIFIER));
    }

    @Test
    public void shouldFollowUserThrowsUnprocessableEntityExceptionWhenUserServiceThrowsUnprocessableEntityException() throws Exception {

        willThrow(UnprocessableEntityException.class).given(userService).followUser(anyString(), anyString());

        mockMvc.perform(post(RequestUrlBuilder.buildFollowUserUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildFollowUserRequestBody(FOLLOW_USER_IDENTIFIER))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        then(userService).should().followUser(eq(USER_IDENTIFIER), eq(FOLLOW_USER_IDENTIFIER));
    }

    @Test
    public void shouldGetTimelineMessagesReturnAllMessagesOfUserFollowingsSuccessfully() throws Exception {

        final List<MessageDTO> messages = new ArrayList<>();
        messages.add(new MessageDTO(new Message("userId", "userIdentifier", "textMessage")));
        given(userService.findTimelineMessages(anyString())).willReturn(messages);

        mockMvc.perform(get(RequestUrlBuilder.buildTimelineUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
        ).andDo(logResultHandler)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.messages.*", hasSize(1)))
                .andExpect(jsonPath("$.messages[0].text", is("textMessage")))
                .andExpect(jsonPath("$.messages[0].userIdentifier", is("userIdentifier")));

        then(userService).should().findTimelineMessages(eq(USER_IDENTIFIER));
    }

    @Test
    public void shouldGetTimelineMessagesThrowExceptionWhenUserServiceThrowsUserIdentifierNotFoundException() throws Exception {

        willThrow(UserIdentifierNotFoundException.class).given(userService).findTimelineMessages(anyString());

        mockMvc.perform(get(RequestUrlBuilder.buildTimelineUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
        ).andDo(logResultHandler)
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        then(userService).should().findTimelineMessages(eq(USER_IDENTIFIER));
    }
}