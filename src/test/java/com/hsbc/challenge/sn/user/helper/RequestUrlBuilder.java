package com.hsbc.challenge.sn.user.helper;

/**
 * Created by 44082315 on 28/01/2018.
 */
public class RequestUrlBuilder {

    private static final String POST_MESSAGE_URI = "/users/{userIdentifier}/messages";
    private static final String FOLLOW_USER_URI = "/users/{userIdentifier}/followings";
    private static final String TIMELINE_URI = "/users/{userIdentifier}/timeline";

    public static String buildPostMessageUrl(final String identifier) {
        return POST_MESSAGE_URI.replace("{userIdentifier}", identifier);
    }

    public static String buildFollowUserUrl(final String identifier) {
        return FOLLOW_USER_URI.replace("{userIdentifier}", identifier);
    }

    public static String buildTimelineUrl(final String identifier) {
        return TIMELINE_URI.replace("{userIdentifier}", identifier);
    }
}
