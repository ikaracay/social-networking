package com.hsbc.challenge.sn.user.service;

import com.hsbc.challenge.sn.exception.UnprocessableEntityException;
import com.hsbc.challenge.sn.exception.UserIdentifierNotFoundException;
import com.hsbc.challenge.sn.message.model.Message;
import com.hsbc.challenge.sn.message.repository.MessageRepository;
import com.hsbc.challenge.sn.user.model.User;
import com.hsbc.challenge.sn.user.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.hsbc.challenge.sn.user.helper.Assertions.assertThatThrows;
import static org.hamcrest.Matchers.isA;
import static org.mockito.BDDMockito.anyList;
import static org.mockito.BDDMockito.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

/**
 * Created by 44082315 on 28/01/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    private static final String USER_IDENTIFIER = "Loremipsu";
    private static final String FOLLOW_USER_IDENTIFIER = "loremip";
    private static final String USER_ID = "888888888";
    private static final String FOLLOWING_USER_ID = "9999999999";
    private static final String POST_MESSAGE_TEXT = "Lorem ipsum dolor sit amet, cons";

    @Autowired
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    MessageRepository messageRepository;

    @Test
    public void shouldPostMessageFirstCreateUserAndThenPostMessageSuccessfully() throws Exception {

        final User user = new User(USER_IDENTIFIER);
        final User savedUser = new User(USER_IDENTIFIER);
        savedUser.setId(USER_ID);
        final Message message = new Message(USER_ID, USER_IDENTIFIER, POST_MESSAGE_TEXT);

        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.empty());
        given(userRepository.save(any(User.class))).willReturn(savedUser);
        given(messageRepository.save(any(Message.class))).willReturn(message);

        userService.postMessage(USER_IDENTIFIER, POST_MESSAGE_TEXT);

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(userRepository).should().save(eq(user));
        then(messageRepository).should().save(eq(message));

    }

    @Test
    public void shouldPostMessageUseExistingUserAndPostMessageSuccessfully() throws Exception {

        final User user = new User(USER_IDENTIFIER);
        user.setId(USER_ID);
        final Message message = new Message(USER_ID, USER_IDENTIFIER, POST_MESSAGE_TEXT);

        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.of(user));
        given(messageRepository.save(any(Message.class))).willReturn(message);

        userService.postMessage(USER_IDENTIFIER, POST_MESSAGE_TEXT);

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(messageRepository).should().save(eq(message));

    }

    @Test
    public void shouldFindMyMessagesReturnUserMessagesSuccessfully() throws Exception {

        final User user = new User(USER_IDENTIFIER);
        user.setId(USER_ID);
        final List<Message> messages = new ArrayList<>();
        messages.add(new Message("userId", "userIdentifier", "textMessage"));

        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.of(user));
        given(messageRepository.findByUserInfo_UserIdOrderByCreatedAtDesc(anyString())).willReturn(messages);

        userService.findMyMessages(USER_IDENTIFIER);

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(messageRepository).should().findByUserInfo_UserIdOrderByCreatedAtDesc(eq(USER_ID));

    }

    @Test
    public void shouldFindMyMessagesThrowUserIdentifierNotFoundExceptionWhenUserNotFound() throws Exception {

        willThrow(UserIdentifierNotFoundException.class).given(userRepository).findByUserIdentifier(anyString());

        assertThatThrows(() -> userService.findMyMessages(USER_IDENTIFIER),
                isA(UserIdentifierNotFoundException.class));

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
    }

    @Test
    public void shouldFindMyTimelineMessagesReturnUserTimelineMessagesSuccessfully() throws Exception {

        final User user = new User(USER_IDENTIFIER);
        user.setId(USER_ID);user.setFollowings(Arrays.asList("xxx", "yyy", "zzz"));
        final List<Message> messages = new ArrayList<>();
        messages.add(new Message("userId", "userIdentifier", "textMessage"));

        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.of(user));
        given(messageRepository.findByUserInfo_UserIdInOrderByCreatedAtDesc(anyList())).willReturn(messages);

        userService.findTimelineMessages(USER_IDENTIFIER);

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(messageRepository).should().findByUserInfo_UserIdInOrderByCreatedAtDesc(eq(user.getFollowings()));

    }

    @Test
    public void shouldFindMyTimelineMessagesThrowUserIdentifierNotFoundExceptionWhenUserNotFound() throws Exception {

        willThrow(UserIdentifierNotFoundException.class).given(userRepository).findByUserIdentifier(anyString());

        assertThatThrows(() -> userService.findTimelineMessages(USER_IDENTIFIER),
                isA(UserIdentifierNotFoundException.class));

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
    }

    @Test
    public void shouldFollowUserSuccessfully() throws Exception {

        final User user = new User(USER_IDENTIFIER);
        final User followingUser = new User(FOLLOW_USER_IDENTIFIER);followingUser.setId(FOLLOWING_USER_ID);

        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.of(user)).willReturn(Optional.of(followingUser));

        userService.followUser(USER_IDENTIFIER, FOLLOW_USER_IDENTIFIER);

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(userRepository).should().findByUserIdentifier(eq(FOLLOW_USER_IDENTIFIER));
        final User afterFollowingUser = new User(USER_IDENTIFIER);

        afterFollowingUser.add2Followings(FOLLOWING_USER_ID);

        then(userRepository).should().save(eq(user));
    }

    @Test
    public void shouldFollowUserThrowUserIdentityNotFoundExceptioinWhenUserNotfound() throws Exception {

        willThrow(UserIdentifierNotFoundException.class).given(userRepository).findByUserIdentifier(anyString());

        assertThatThrows(() -> userService.followUser(USER_IDENTIFIER, FOLLOW_USER_IDENTIFIER),
                isA(UserIdentifierNotFoundException.class));

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
    }

    @Test
    public void shouldFollowUserThrowUserIdentityNotFoundExceptioinWhenFollowingUserNotfound() throws Exception {

        final User user = new User(USER_IDENTIFIER);
        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.of(user)).willThrow(UserIdentifierNotFoundException.class);

        assertThatThrows(() -> userService.followUser(USER_IDENTIFIER, FOLLOW_USER_IDENTIFIER),
                isA(UserIdentifierNotFoundException.class));

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(userRepository).should().findByUserIdentifier(eq(FOLLOW_USER_IDENTIFIER));
    }

    @Test
    public void shouldFollowUserThrowUnprocessableEntityExceptionWhenFollowingUserHasAlreadyAdded() throws Exception {

        final User user = new User(USER_IDENTIFIER);user.add2Followings(FOLLOWING_USER_ID);
        final User followingUser = new User(FOLLOW_USER_IDENTIFIER);followingUser.setId(FOLLOWING_USER_ID);

        given(userRepository.findByUserIdentifier(anyString())).willReturn(Optional.of(user)).willReturn(Optional.of(followingUser));

        assertThatThrows(() -> userService.followUser(USER_IDENTIFIER, FOLLOW_USER_IDENTIFIER),
                isA(UnprocessableEntityException.class));

        then(userRepository).should().findByUserIdentifier(eq(USER_IDENTIFIER));
        then(userRepository).should().findByUserIdentifier(eq(FOLLOW_USER_IDENTIFIER));
    }

}