package com.hsbc.challenge.sn.user.helper;


import org.hamcrest.Matcher;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Assertions {

    @SuppressWarnings("unchecked")
    public static void assertThatThrows(CallableWithExceptions callable, Matcher matcher) {
        boolean doThrow = true;
        try {
            callable.call();
            doThrow = false;
        } catch (Throwable throwable) {
            assertThat(throwable, matcher);
        }
        assertThat("Call did not thrown", doThrow, is(true));

    }

    @FunctionalInterface
    public interface CallableWithExceptions {
        void call() throws Throwable;
    }
}
