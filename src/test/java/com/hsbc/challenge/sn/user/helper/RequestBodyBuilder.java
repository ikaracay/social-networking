package com.hsbc.challenge.sn.user.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsbc.challenge.sn.user.model.FollowUserRequest;
import com.hsbc.challenge.sn.user.model.PostMessageRequest;

/**
 * Created by 44082315 on 28/01/2018.
 */
public class RequestBodyBuilder {

    public static String buildPostMessageRequestBody(final String text) throws JsonProcessingException {

        final ObjectMapper mapper = new ObjectMapper();
        final PostMessageRequest request = new PostMessageRequest(text);

        return mapper.writeValueAsString(request);
    }

    public static String buildFollowUserRequestBody(final String userIdentifier) throws JsonProcessingException {

        final ObjectMapper mapper = new ObjectMapper();
        final FollowUserRequest request = new FollowUserRequest(userIdentifier);

        return mapper.writeValueAsString(request);
    }
}
