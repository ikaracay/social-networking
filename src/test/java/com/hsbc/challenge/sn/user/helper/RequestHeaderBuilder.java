package com.hsbc.challenge.sn.user.helper;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by 44082315 on 28/01/2018.
 */
public class RequestHeaderBuilder {

    public static HttpHeaders buildHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Accept", "application/json");
        return headers;
    }
}
