package com.hsbc.challenge.sn.user.controller;

import com.hsbc.challenge.sn.user.helper.RequestBodyBuilder;
import com.hsbc.challenge.sn.user.helper.RequestHeaderBuilder;
import com.hsbc.challenge.sn.user.helper.RequestUrlBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by 44082315 on 28/01/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerContractValidationTest {

    private static final Logger logger = LoggerFactory.getLogger(UserControllerContractValidationTest.class);
    private static final ResultHandler logResultHandler = result -> logger.info(result.getResponse().getContentAsString());

    private static final String USER_IDENTIFIER = "ilter";
    private static final String INVALID_USER_IDENTIFIER = " ";
    private static final String EMPTY_STRING = "";
    private static final String INVALID_USER_IDENTIFIER_MESSAGE = "invalid userIdentifier";
    private static final String POST_MESSAGE_TEXT = "Lorem ipsu";
    private static final String NULL_OR_EMPTY_ERROR_MESSAGE = "Null or Empty message";
    private static final String NULL_OR_EMPTY_IDENTIFIER_MESSAGE = "Null or Empty userIdentifier";
    private static final String POST_MESSAGE_TEXT_LONGER_THAN_140_CHARACTER = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et mag";
    private static final String POST_MESSAGE_TEXT_LONGER_THAN_140_CHARACTER_ERROR_MESSAGE = "Message text longer than 140 character";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldPostMessageThrowUnprocessableEntityExceptionWhenUserIdentifierIsInvalid() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildPostMessageUrl(INVALID_USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(POST_MESSAGE_TEXT))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(INVALID_USER_IDENTIFIER_MESSAGE)));
    }

    @Test
    public void shouldPostMessageThrowUnprocessableEntityExceptionWhenTextMessageIsNull() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(null))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(NULL_OR_EMPTY_ERROR_MESSAGE)));
    }

    @Test
    public void shouldPostMessageThrowUnprocessableEntityExceptionWhenTextMessageIsEmptyString() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(EMPTY_STRING))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(NULL_OR_EMPTY_ERROR_MESSAGE)));
    }

    @Test
    public void shouldPostMessageThrowUnprocessableEntityExceptionWhenTextMessageIsLongerThan140Character() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildPostMessageUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildPostMessageRequestBody(POST_MESSAGE_TEXT_LONGER_THAN_140_CHARACTER))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(POST_MESSAGE_TEXT_LONGER_THAN_140_CHARACTER_ERROR_MESSAGE)));
    }

    @Test
    public void shouldGetMyMessagesThrowsUnprocessableEntityExceptionWhenUserIdentifierIsInvalid() throws Exception {

        mockMvc.perform(get(RequestUrlBuilder.buildPostMessageUrl(INVALID_USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(INVALID_USER_IDENTIFIER_MESSAGE)));
    }

    @Test
    public void shouldFollowUserThrowsUnprocessableEntityExceptionWhenUserIdentifierIsInvalid() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildFollowUserUrl(INVALID_USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildFollowUserRequestBody(USER_IDENTIFIER))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(INVALID_USER_IDENTIFIER_MESSAGE)));
    }

    @Test
    public void shouldFollowUserThrowsUnprocessableEntityExceptionWhenFollowingUserIdentifierIsNull() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildFollowUserUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildFollowUserRequestBody(null))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(NULL_OR_EMPTY_IDENTIFIER_MESSAGE)));
    }

    @Test
    public void shouldFollowUserThrowsUnprocessableEntityExceptionWhenFollowingUserIdentifierIsEmpty() throws Exception {

        mockMvc.perform(post(RequestUrlBuilder.buildFollowUserUrl(USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
                .content(RequestBodyBuilder.buildFollowUserRequestBody(EMPTY_STRING))
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(NULL_OR_EMPTY_IDENTIFIER_MESSAGE)));
    }

    @Test
    public void shouldGetTimelineMessagesThrowsUnprocessableEntityExceptionWhenUserIdentifierIsInvalid() throws Exception {

        mockMvc.perform(get(RequestUrlBuilder.buildTimelineUrl(INVALID_USER_IDENTIFIER))
                .headers(RequestHeaderBuilder.buildHeaders())
        ).andDo(logResultHandler)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errorCode", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.errorDescription", is(INVALID_USER_IDENTIFIER_MESSAGE)));
    }

}