package com.hsbc.challenge.sn.exception;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class UserIdentifierNotFoundException extends RuntimeException {
    public UserIdentifierNotFoundException(final String message) {
        super(message);
    }
}
