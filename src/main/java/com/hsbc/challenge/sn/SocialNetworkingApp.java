package com.hsbc.challenge.sn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 44082315 on 27/01/2018.
 */
@SpringBootApplication
public class SocialNetworkingApp {

    public static void main(String[] args) {
        SpringApplication.run(SocialNetworkingApp.class, args);
    }
}
