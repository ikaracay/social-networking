package com.hsbc.challenge.sn.config;

import com.hsbc.challenge.sn.message.repository.MessageRepository;
import com.hsbc.challenge.sn.user.repository.UserRepository;
import com.hsbc.challenge.sn.user.service.UserService;
import com.hsbc.challenge.sn.user.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

/**
 * Created by 44082315 on 27/01/2018.
 */
@Configuration
@EnableMongoAuditing
public class AppConfig {

    @Bean
    public UserService userService(final UserRepository userRepository, final MessageRepository messageRepository) {
        return new UserServiceImpl(userRepository, messageRepository);
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }
}
