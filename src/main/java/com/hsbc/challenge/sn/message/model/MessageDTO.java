package com.hsbc.challenge.sn.message.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class MessageDTO {

    private String text;
    private String userIdentifier;
    private LocalDateTime createdAt;

    public MessageDTO(final Message message) {
        this.text = message.getText();
        this.userIdentifier = message.getUserInfo().getUserIdentifier();
        this.createdAt = message.getCreatedAt();
    }

    public String getText() {
        return text;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
}
