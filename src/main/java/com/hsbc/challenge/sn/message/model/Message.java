package com.hsbc.challenge.sn.message.model;

import com.hsbc.challenge.sn.user.model.UserInfo;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Created by 44082315 on 27/01/2018.
 */
@Document
public class Message {
    @Id
    private String id;
    private String text;
    @CreatedDate
    private LocalDateTime createdAt;

    //Using NoSQL database advantage. Applying DeNormalization to read data without needed unnecessary join
    private UserInfo userInfo;

    public Message() {
    }

    public Message(final String userId, final String userIdentifier, final String text) {
        this.text = text;
        this.userInfo = new UserInfo(userId, userIdentifier);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(id, message.id) &&
                Objects.equals(text, message.text) &&
                Objects.equals(createdAt, message.createdAt) &&
                Objects.equals(userInfo, message.userInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, createdAt, userInfo);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", createdAt=" + createdAt +
                ", userInfo=" + userInfo +
                '}';
    }
}
