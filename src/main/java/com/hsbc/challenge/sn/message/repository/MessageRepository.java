package com.hsbc.challenge.sn.message.repository;

import com.hsbc.challenge.sn.message.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by 44082315 on 27/01/2018.
 */
@Repository
public interface MessageRepository extends MongoRepository<Message, String> {

    List<Message> findByUserInfo_UserIdOrderByCreatedAtDesc(final String id);

    List<Message> findByUserInfo_UserIdInOrderByCreatedAtDesc(final List<String> ids);
}
