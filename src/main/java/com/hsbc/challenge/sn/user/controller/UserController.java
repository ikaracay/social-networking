package com.hsbc.challenge.sn.user.controller;

import com.hsbc.challenge.sn.exception.UnprocessableEntityException;
import com.hsbc.challenge.sn.message.model.MessageDTO;
import com.hsbc.challenge.sn.user.model.FollowUserRequest;
import com.hsbc.challenge.sn.user.model.MessageResponse;
import com.hsbc.challenge.sn.user.model.PostMessageRequest;
import com.hsbc.challenge.sn.user.model.SNResponse;
import com.hsbc.challenge.sn.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Created by 44082315 on 27/01/2018.
 */
@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/{userIdentifier}/messages")
    @ResponseStatus(HttpStatus.CREATED)
    public SNResponse postMessage(@Pattern(regexp = "[a-zA-Z0-9-]+", message = "invalid userIdentifier") @PathVariable("userIdentifier") final String userIdentifier,
                                  @Valid @RequestBody final PostMessageRequest request,
                                  final BindingResult validationResults) {

        LOGGER.info("starting postMessage");

        if (validationResults.hasErrors()) {
            final String rejectedMessage = validationResults.getAllErrors().get(0).getDefaultMessage();
            throw new UnprocessableEntityException(rejectedMessage);
        }

        userService.postMessage(userIdentifier, request.getMessage());

        LOGGER.info("message posted");

        return new SNResponse("ok");
    }

    @GetMapping(path = "/{userIdentifier}/messages")
    public MessageResponse getMyMessages(@Pattern(regexp = "[a-zA-Z0-9-]+", message = "invalid userIdentifier") @PathVariable("userIdentifier") final String userIdentifier) {

        LOGGER.info("starting getMyMessages");

        final List<MessageDTO> messages = userService.findMyMessages(userIdentifier);

        LOGGER.info("wall messages retrieved");

        return new MessageResponse(messages);
    }

    @PostMapping(path = "/{userIdentifier}/followings")
    public SNResponse followUser(@Pattern(regexp = "[a-zA-Z0-9-]+", message = "invalid userIdentifier") @PathVariable("userIdentifier") final String userIdentifier,
                                 @Valid @RequestBody final FollowUserRequest request,
                                 final BindingResult validationResults) {

        LOGGER.info("starting followUser");

        if (validationResults.hasErrors()) {
            final String rejectedMessage = validationResults.getAllErrors().get(0).getDefaultMessage();
            throw new UnprocessableEntityException(rejectedMessage);
        }

        userService.followUser(userIdentifier, request.getUserIdentifier());

        LOGGER.info("user followed");

        return new SNResponse("ok");
    }

    @GetMapping(path = "/{userIdentifier}/timeline")
    public MessageResponse getTimelineMessages(@Pattern(regexp = "[a-zA-Z0-9-]+", message = "invalid userIdentifier") @PathVariable("userIdentifier") final String userIdentifier) {

        LOGGER.debug("starting getTimelineMessages");

        final List<MessageDTO> timelineMessages = userService.findTimelineMessages(userIdentifier);

        LOGGER.info("timeline messages retrieved");

        return new MessageResponse(timelineMessages);
    }
}
