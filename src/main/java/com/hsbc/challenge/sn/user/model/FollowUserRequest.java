package com.hsbc.challenge.sn.user.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class FollowUserRequest {

    @NotEmpty(message = "Null or Empty userIdentifier")
    private final String userIdentifier;

    @JsonCreator
    public FollowUserRequest(@JsonProperty("userIdentifier") final String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }
}
