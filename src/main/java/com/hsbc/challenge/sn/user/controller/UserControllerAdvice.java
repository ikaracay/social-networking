package com.hsbc.challenge.sn.user.controller;

import com.hsbc.challenge.sn.exception.UnprocessableEntityException;
import com.hsbc.challenge.sn.exception.UserIdentifierNotFoundException;
import com.hsbc.challenge.sn.user.error.StandardError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * Created by 44082315 on 27/01/2018.
 */
@ControllerAdvice
@ResponseBody
public class UserControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserControllerAdvice.class);

    private static final String SORRY_SOMETHING_WENT_WRONG = "Sorry, something went wrong.";
    private static final String BAD_REQUEST = "Bad Request";

    // Generic
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    public StandardError generic(final Exception e) {
        LOGGER.error(e.getLocalizedMessage(), e);

        return error(HttpStatus.INTERNAL_SERVER_ERROR, SORRY_SOMETHING_WENT_WRONG);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public StandardError unprocessableJSONRequest(final HttpMessageNotReadableException e) {
        LOGGER.error(e.getLocalizedMessage(), e);

        return error(HttpStatus.BAD_REQUEST, BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({UserIdentifierNotFoundException.class})
    public StandardError userIdentifierNotFound(final UserIdentifierNotFoundException e) {
        LOGGER.error(e.getLocalizedMessage(), e);

        return error(HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({UnprocessableEntityException.class})
    public StandardError unprocessableEntity(final UnprocessableEntityException e) {
        LOGGER.error("unprocessableEntity:" + e.getLocalizedMessage(), e);

        return error(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({ConstraintViolationException.class})
    public StandardError constraintViolationException(final ConstraintViolationException e) {
        LOGGER.error("constraintViolationException:" + e.getLocalizedMessage(), e);
        StringBuilder messages = new StringBuilder();
        for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
            messages.append(violation.getMessage());
        }
        return error(HttpStatus.UNPROCESSABLE_ENTITY, messages.toString());
    }

    private StandardError error(final HttpStatus errorCode, final String errorDescription) {
        return new StandardError(errorCode.value(), errorDescription);
    }
}
