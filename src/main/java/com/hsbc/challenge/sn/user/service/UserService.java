package com.hsbc.challenge.sn.user.service;

import com.hsbc.challenge.sn.message.model.MessageDTO;

import java.util.List;

/**
 * Created by 44082315 on 27/01/2018.
 */
public interface UserService {

    void postMessage(final String userIdentifier, final String text);

    List<MessageDTO> findMyMessages(final String userIdentifier);

    List<MessageDTO> findTimelineMessages(final String userIdentifier);

    void followUser(final String userIdentifier, final String followingUserIdentifier);
}
