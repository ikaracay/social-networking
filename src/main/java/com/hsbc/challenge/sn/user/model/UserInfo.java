package com.hsbc.challenge.sn.user.model;

import java.util.Objects;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class UserInfo {
    private String userId;
    //Use NoSQL database advantage. Apply DeNormalization to avoid unnecessary join
    private String userIdentifier;

    public UserInfo(final String userId, final String userIdentifier) {
        this.userId = userId;
        this.userIdentifier = userIdentifier;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return Objects.equals(userId, userInfo.userId) &&
                Objects.equals(userIdentifier, userInfo.userIdentifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userIdentifier);
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userId='" + userId + '\'' +
                ", userIdentifier='" + userIdentifier + '\'' +
                '}';
    }
}
