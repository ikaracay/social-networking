package com.hsbc.challenge.sn.user.service;

import com.hsbc.challenge.sn.exception.UnprocessableEntityException;
import com.hsbc.challenge.sn.exception.UserIdentifierNotFoundException;
import com.hsbc.challenge.sn.message.model.Message;
import com.hsbc.challenge.sn.message.model.MessageDTO;
import com.hsbc.challenge.sn.message.repository.MessageRepository;
import com.hsbc.challenge.sn.user.model.User;
import com.hsbc.challenge.sn.user.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final MessageRepository messageRepository;

    public UserServiceImpl(final UserRepository userRepository, final MessageRepository messageRepository) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    @Override
    public void postMessage(final String userIdentifier, final String text) {

        final User user = userRepository.findByUserIdentifier(userIdentifier)
                .orElseGet(() -> userRepository.save(new User(userIdentifier)));

        messageRepository.save(new Message(user.getId(), user.getUserIdentifier(), text));
    }

    @Override
    public List<MessageDTO> findMyMessages(final String userIdentifier) {
        return userRepository.findByUserIdentifier(userIdentifier)
                .map(user -> messageRepository.findByUserInfo_UserIdOrderByCreatedAtDesc(user.getId())
                        .stream()
                        .map(MessageDTO::new)
                        .collect(Collectors.toList()))
                .orElseThrow(() -> new UserIdentifierNotFoundException("userIdentifier not found"));
    }

    @Override
    public List<MessageDTO> findTimelineMessages(final String userIdentifier) {
        return userRepository.findByUserIdentifier(userIdentifier)
                .map(user -> messageRepository.findByUserInfo_UserIdInOrderByCreatedAtDesc(user.getFollowings())
                        .stream()
                        .map(MessageDTO::new)
                        .collect(Collectors.toList()))
                .orElseThrow(() -> new UserIdentifierNotFoundException("userIdentifier not found"));
    }

    @Override
    public void followUser(final String userIdentifier, final String followingUserIdentifier) {

        final User user = userRepository.findByUserIdentifier(userIdentifier)
                .orElseThrow(() -> new UserIdentifierNotFoundException("userIdentifier not found"));

        final User followingUser = userRepository.findByUserIdentifier(followingUserIdentifier)
                .orElseThrow(() -> new UserIdentifierNotFoundException("userIdentifier not found"));

        if (user.getFollowings().contains(followingUser.getId())) {
            throw new UnprocessableEntityException("User has already followed the requested user");
        }

        user.add2Followings(followingUser.getId());

        userRepository.save(user);
    }
}
