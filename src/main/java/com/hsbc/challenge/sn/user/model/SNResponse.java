package com.hsbc.challenge.sn.user.model;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class SNResponse {

    private final String result;

    public SNResponse(final String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
