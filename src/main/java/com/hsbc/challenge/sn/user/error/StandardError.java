package com.hsbc.challenge.sn.user.error;

public class StandardError {

    private int errorCode;
    private String errorDescription;

    public StandardError(final int errorCode, final String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
