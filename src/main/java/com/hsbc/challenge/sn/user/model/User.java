package com.hsbc.challenge.sn.user.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by 44082315 on 27/01/2018.
 */
@Document
public class User {

    @Id
    private String id;

    private String userIdentifier;

    private List<String> followings;

    @CreatedDate
    private LocalDateTime createdAt;

    public User() {
    }

    public User(final String userIdentifier) {
        this.userIdentifier = userIdentifier;
        followings = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public List<String> getFollowings() {
        return followings;
    }

    public void setFollowings(List<String> followings) {
        this.followings = followings;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void add2Followings(final String userId) {
        followings.add(userId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(userIdentifier, user.userIdentifier) &&
                Objects.equals(followings, user.followings) &&
                Objects.equals(createdAt, user.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userIdentifier, followings, createdAt);
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", userIdentifier='" + userIdentifier + '\'' +
                ", followings=" + followings +
                ", createdAt=" + createdAt +
                '}';
    }
}
