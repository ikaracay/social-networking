package com.hsbc.challenge.sn.user.repository;

import com.hsbc.challenge.sn.user.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by 44082315 on 27/01/2018.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByUserIdentifier(final String userIdentifier);
}
