package com.hsbc.challenge.sn.user.model;

import com.hsbc.challenge.sn.message.model.MessageDTO;

import java.util.List;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class MessageResponse {
    private List<MessageDTO> messages;

    public MessageResponse(List<MessageDTO> messages) {
        this.messages = messages;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) {
        this.messages = messages;
    }
}
