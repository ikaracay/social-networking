package com.hsbc.challenge.sn.user.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by 44082315 on 27/01/2018.
 */
public class PostMessageRequest {
    @NotEmpty(message = "Null or Empty message")
    @Size(max = 140, message = "Message text longer than 140 character")
    private final String message;

    @JsonCreator
    public PostMessageRequest(@JsonProperty("message") final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
