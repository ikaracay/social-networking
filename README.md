This project has been developed to simulate social networking application similar to Twitter.
Java 8, Spring Boot, Fongo(in memory java implementation of MongoDB) have been used in this project. 
Mockito BDD and Hamcrest libraries have been used to develop the test scenarios. 
 
Sample Postman collection and curl commands can be found under files directory. The files directory contains the following files:

1)curl-sample-requests.txt

2)social-networking.postman_collection.json

Rest API

UserController is the main Rest Controller of the project. The four different methods exist under Rest Controller are given below;

1)postMessage -> used for post new message

Sample request: userIdentifier1 is the user that wants to post message

curl -X POST \
  http://localhost:8080/users/userIdentifier1/messages \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{"message":"first message"}'

2)getMyMessages -> used for to return user messages(Wall)

Sample request: userIdentifier1 is the user to get wall messages

curl -X GET \
  http://localhost:8080/users/userIdentifier1/messages \
  -H 'cache-control: no-cache'

3)followUser -> used for to follow another user

Sample request: userIdentifier1 is the user to follow another user. userIdentifier1 is the user that userIdentifier1 wants to follow.

curl -X POST \
  http://localhost:8080/users/userIdentifier1/followings \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{"userIdentifier":"userIdentifier2"}'

4)getTimelineMessages -> used for to return user timeline messages(Timeline)

Sample request: userIdentifier1 is the user to get timeline messages.

curl -X GET \
  http://localhost:8080/users/userIdentifier1/timeline \
  -H 'cache-control: no-cache'
  
Test

3 test classes have been developed to cover all scenarios for both controller and service layers. Test cases are developed with Mockito BDD(Behaviour driven development) library. These are:

1)UserControllerMockTest

All the success and error cases have been tested in this controller layer. 9 different test cases exist in this class.

2)UserControllerConractValidationTest

All request parameters and input objects are secured by regular expressions in the application. All error
cases have been tested in this class. 9 different test cases exist in this class.

3)UserServiceTest

All business logic has been developed in service layer. 10 different test cases have been developed in this test class

To run the application:

From terminal, go to project folder and run:

mvn spring-boot:run